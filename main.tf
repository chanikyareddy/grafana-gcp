provider "google" {
credentials = "${file("vistadevops-3c603128d723.json")}"
project     = "vistadevops"
region      = "us-east4"
}

resource "random_id" "instance_id" {
byte_length = 8
}

resource "google_compute_instance" "nucuta" {
name = "nucuta-vm-${random_id.instance_id.hex}"
machine_type = "e2-medium"
zone = "us-east4-a"

boot_disk {
initialize_params {
image = "ubuntu-os-cloud/ubuntu-1604-lts"
}
}
 
metadata_startup_script = "sudo apt-get -y update; wget https://dl.grafana.com/oss/release/grafana_6.5.2_amd64.deb; sudo apt-get install -y adduser libfontconfig; sudo apt --fix-broken install; sudo dpkg -i grafana_6.5.2_amd64.deb; sh -c 'echo 'deb https://packagecloud.io/grafana/stable/debian/ stretch main' >> /etc/apt/sources.list'; curl https://packagecloud.io/gpg.key | sudo apt-key add -; sudo apt-get update; sudo apt-get install grafana; sudo service grafana-server start; sudo update-rc.d grafana-server defaults; sudo systemctl enable grafana-server.service"
 
network_interface {
network = "default"
 
access_config {
} 
}

 
metadata = {
sshKeys = "chani:${file("dilanga.pub")}"
}
}
 
resource "google_compute_firewall" "default" {
name    = "grafana-firewall"
network = "default"
 
allow {
protocol = "tcp"
ports    = ["3000","443"]
}
 
allow {
protocol = "icmp"
}
}
 
output "ip" {
value = "${google_compute_instance.nucuta.network_interface.0.access_config.0.nat_ip}"
}